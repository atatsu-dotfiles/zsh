# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

[ ! -d "${HOME}/downloads" ] && mkdir "${HOME}/downloads"

if whence dircolors >/dev/null; then
	eval `dircolors -b`
else
	export CLICOLOR=1
fi

# {{{ Aliases

# {{{ Auto extension
#alias -s py=$EDITOR
alias -s yml=$EDITOR
alias -s png=feh
alias -s jpg=feh
alias -s gif=feh
# }}}

# {{{ Main
if whence dircolors >/dev/null; then
	alias ls='ls -F --color=always'
else
	alias ls='ls -F'
fi
alias diff='diff --color=auto'
alias ll='ls -lh'
alias la='ls -a'
alias lst="tree -I 'virtualenv|node_modules|bower_components|__pycache__'"
alias ping='ping -c 4'
alias mem='free -m'
alias dh='dirs -v'
alias grep='grep --color=auto'
alias sgrep='grep --color=auto -rn --exclude-dir=.svn --exclude-dir=virtualenv --exclude-dir=node_modules'
alias screenshot='grim -g "$(slurp)"'

# {{{ SSH
# }}}

# {{{ Pacman
# }}}

# {{{ git
alias st='git status'
alias br='git branch'
alias bra='git branch -a'
alias brd='git branch -d'
alias brD='git branch -D'
alias sw='git checkout'
alias swc='git checkout -b'
alias gl='git log --graph --color --abbrev-commit --pretty=format:"%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr)%C(bold blue) <%an> %Creset"'
alias glast='git log -1 HEAD'
# }}}

# }}}

# {{{ virtualenv
# searches for the existence of a virtualenv and if found activates it
# will traverse upwards through the filesystem until either one is found
# or root is reached
virt () {
	local start_dir=`pwd`
	while [[ `pwd` != '/' ]] {
		if [[ (-e virtualenv/bin/activate) ]] {
			local cwd=`pwd`
			#VIRTUAL_ENV_PY="$cwd/virtualenv/bin/python" nvim $@
			echo "virtualenv found in $cwd, activating"
			source "$cwd/virtualenv/bin/activate" 
			cd $start_dir
			return 0
		} 
		cd ../
	}
	cd $start_dir
	echo 'no virtualenv found'
	return 1
}
# }}}

# {{{ tmux playing nicely with ssh-agent
# if [[ -z "$TMUX" ]] {
# 	# not in a tmux session
#
# 	if [[ -z "$SSH_AUTH_SOCK" ]] {
# 		# ssh auth variable is missing
# 		export SSH_AUTH_SOCK="$HOME/.ssh/.auth_socket"
# 	}
# 	if [[ ! -S "$SSH_AUTH_SOCK" ]] {
# 		# socket is available so create the new auth session
# 		eval `ssh-agent -a $SSH_AUTH_SOCK` > /dev/null 2>&1
# 		echo $SSH_AGENT_PID > $HOME/.ssh/.auth_pid
# 	}
#
# 	if [[ -z $SSH_AGENT_PID ]] {
# 		# agent isn't defined so recreate it from pid file
# 		export SSH_AGENT_PID=`cat $HOME/.ssh/.auth_pid`
# 	}
# } else {
# 	# we are in a tmux session
# 	if [[ -z "$SSH_AUTH_SOCK" ]] {
# 		export SSH_AUTH_SOCK="$HOME/.ssh/.auth_socket"
# 	}
# }
# }}}

# {{{ Keybindings
bindkey -v
bindkey "\e[1~" beginning-of-line
bindkey "\e[4~" end-of-line
bindkey "\e[5~" beginning-of-history
bindkey "\e[6~" end-of-history
bindkey "\e[3~" delete-char
bindkey "\e[2~" quoted-insert
bindkey "\e[5C" forward-word
bindkey "\eOc" emacs-forward-word
bindkey "\e[5D" backward-word
bindkey "\eOd" emacs-backward-word
bindkey "\e\e[C" forward-word
bindkey "\e\e[D" backward-word
bindkey "^H" backward-delete-word
bindkey "^R" history-incremental-search-backward
bindkey "^[[A" history-search-backward
bindkey "^[[B" history-search-forward
# }}}

# }}}

# {{{ keychain
# if whence keychain >/dev/null; then
#   eval $(keychain --eval --quiet id_rsa)
# else
#   echo "`keychain` not installed"
# fi
# }}}

# {{{ ZSH Settings
# Prompt requirements
setopt autopushd pushdminus pushdsilent pushdtohome

# New style completion system
autoload -U compinit; compinit
#  * List of completers to use
zstyle ":completion:*" completer _complete _match _approximate
#  * Allow approximate
zstyle ":completion:*:match:*" original only
zstyle ":completion:*:approximate:*" max-errors 1 numeric
#  * Selection prompt as menu
zstyle ":completion:*" menu select=1
#  * Menu selection for PID completion
zstyle ":completion:*:*:kill:*" menu yes select
zstyle ":completion:*:kill:*" force-list always
zstyle ":completion:*:processes" command "ps -au$USER"
zstyle ":completion:*:*:kill:*:processes" list-colors "=(#b) #([0-9]#)*=0=01;32"
#  * Don't select parent dir on cd
zstyle ":completion:*:cd:*" ignore-parents parent pwd
#  * Complete with colors
zstyle ":completion:*" list-colors ""
#  * type a directory's name to cd to it
#compctl -/ cd
# }}}

# {{{ fzf overrides
_fzf_compgen_path() {
	ag -g "" "$1"
}
# }}}

# {{{ Zplug
# Assumes zplug was installed from AUR
if [[ ! -d "/usr/share/zsh/scripts/zplug" ]] {
	echo 'Install zplug and then "zplug install"'
} else {
	source /usr/share/zsh/scripts/zplug/init.zsh
	zplug 'romkatv/powerlevel10k', as:theme, depth:1
	zplug load
}
# }}}

# To customize prompt, run `p10k configure` or edit ~/dotfiles/zsh/p10k.zsh.
[ -f "${HOME}/dotfiles/zsh/p10k.zsh" ] && source "${HOME}/dotfiles/zsh/p10k.zsh"

# The one line syntax actually results in an error code (apparently) if the
# test fails so we lay it out in multiline fashion
if [[ -f "${HOME}/.fzf.zsh" ]] {
	source "${HOME}/.fzf.zsh"
} elif [[ -f /usr/share/fzf/completion.zsh ]] {
  source /usr/share/fzf/completion.zsh
  source /usr/share/fzf/key-bindings.zsh
}

if [[ -d "${HOME}/stl/prefix" ]] {
}

# To customize prompt, run `p10k configure` or edit ~/dotfiles/zsh/p10k.zsh.
[[ ! -f ~/dotfiles/zsh/p10k.zsh ]] || source ~/dotfiles/zsh/p10k.zsh
